# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import serial as sr
import matplotlib.pyplot as plt
import numpy

s = sr.Serial('COM5',9600) 
plt.close('all')
plt.figure()
plt.ion()
plt.show()
data = numpy.array([])
i=0
while i<100:
    a= s.readline() 
    a.decode()
    b=float(a[0:4])
    data = numpy.append(data, b)
    plt.cla()
    plt.plot(data)
    plt.pause(0.01)
    i=i+1